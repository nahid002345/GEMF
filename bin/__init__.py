from DataImport import import_data_graph, import_data_adj_matrix, import_data_array, import_labels, import_labels_array, \
    pad_to_square
from MatrixFactorization import svd_factorization_dense, pmf_factorization, pmf_factorization_hailin, \
    pmf_factorization_chy, svd_factorization_sparse
import networkx as nx
import numpy as np

np.set_printoptions(threshold=np.nan)
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from PlotGraphVisual import plot_embedding2D
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LogisticRegressionCV, LogisticRegression
from scipy import sparse
from sklearn.preprocessing import StandardScaler
import pandas as pd
from sklearn import model_selection,cross_validation
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, classification_report, \
    confusion_matrix
import time


def user_input():
    print("Select your Dataset \n Karate : 1 \n Lesmis : 2 \n Blog Catalog: 3")
    dataset_choice = input("Please enter your choice: ")
    is_sparse = input("Do the embedding for Sparse Matrix or Dense Matrix (s = sparse matrix / d = dense matrix): ")
    print("Select matrix factorization \n PMF : 1 \n SVD : 2 ")
    matrix_factorization_choice = input("Please enter your matrix factorization choice : ")
    if (dataset_choice != '3'):
        is_show_plot = input("Show the embedded plot? (y/n) : ")
        is_classification = 'y'
    else:
        is_show_plot = 'n'
        is_classification = input("Do Classification for selected embedded graph? (y/n) : ")
    return dataset_choice, is_show_plot, is_sparse, matrix_factorization_choice, is_classification


def data_processing_to_matrix(user_dataset_choice):
    # put dataset file in dataset folder
    KARATE_DATASET = "out.ucidata-zachary"
    LESMIS_DATASET = "out.moreno_lesmis_lesmis"
    BLOGCATALOG_DATASET = "out.edges"

    if user_dataset_choice == '1':
        INSERTED_DATASET = KARATE_DATASET
    elif user_dataset_choice == '2':
        INSERTED_DATASET = LESMIS_DATASET
    elif user_dataset_choice == '3':
        INSERTED_DATASET = BLOGCATALOG_DATASET
    else:
        INSERTED_DATASET = KARATE_DATASET

    dataset_name = [k for k, v in locals().items() if v == INSERTED_DATASET][0]
    print_result.append(dataset_name)
    # import adjacency matrix from dataset
    if user_dataset_choice == '3':
        adj_matrix, node, edge = import_data_adj_matrix(BLOGCATALOG_DATASET, dataset_level=dataset_name, weighted=False,
                                                        delimiter=",")
    else:
        adj_matrix, node, edge = import_data_adj_matrix(INSERTED_DATASET, dataset_level=dataset_name, weighted=True)

    print_result.append(node)
    print_result.append(edge)
    return adj_matrix, dataset_name


def matrix_factorization(adj_matrix, is_sparse, matrix_factorization_choice):
    s = []
    if is_sparse == 'd':
        adj_matrix = adj_matrix.todense()
        if matrix_factorization_choice == '1':
            ###PMF
            u, v = pmf_factorization(adj_matrix)
            G1 = nx.from_numpy_matrix(np.array(u))

            s = u[0, :]

            print(s)
        else:
            ###SVD
            u, s, v = svd_factorization_dense(adj_matrix)
            u_pad = pad_to_square(u)
            G1 = nx.from_numpy_matrix(u_pad, create_using=nx.MultiGraph())
    else:
        if matrix_factorization_choice == '1':
            ###PMF
            u, v = pmf_factorization(adj_matrix)
            G1 = nx.from_numpy_matrix(np.array(u))
            s = u[0, :]
            print(s)
        else:
            ###SVD

            u, s, v = svd_factorization_sparse(adj_matrix)
            u_pad = pad_to_square(u)
            G1 = nx.from_numpy_matrix(u_pad, create_using=nx.MultiGraph())

    if all(i == 0 for i in s.astype(int)):
        s = s * 100

    print_result.append(is_sparse)
    print_result.append(matrix_factorization_choice)
    return u, v, s, G1


def matrix_factorization_classification(u_vec, s_vec):
    # Classification task
    y = import_labels_array("nodes.csv")
    y = s_vec
    y = np.ravel(y)

    print(y.shape)
    # X = u_vec[:,:5]
    X = u_vec
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5)
    y_train = y_train.astype(int)
    y_test = y_test.astype(int)
    print(X_train.shape)
    print(y_train.shape)

    print("data")
    print(y_train)
    print(y_test)

    logistic_regression = LogisticRegression()
    # logistic_regression.fit(X_train, y_train)
    predicted = cross_validation.cross_val_predict(logistic_regression, X_train, y_train, cv=10)
    matrix = confusion_matrix(y_test, predicted)

    f1 = f1_score(y_test, predicted, average="macro")
    precision = precision_score(y_test, predicted, average="macro")
    recall = recall_score(y_test, predicted, average="macro")
    print_result.append(f1)
    print_result.append(precision)
    print_result.append(recall)
    print(matrix)
    print(f1)
    print(precision)
    print(recall)
    process_time = (time.time() - start_time)
    print("--- %s seconds ---" % process_time)
    print_result.append(process_time)

    df = pd.DataFrame(columns=['dataset', 'node', 'edge', 'm_type', 'factrz_type', 'f1', 'precision', 'recall', 'time'])
    df.loc[0] = print_result

    df.to_csv('results.csv', sep=',', header=False, index=True, mode='a')


def show_plot(u_vec, G1, dataset_name):
    if is_sparse == 'd':
        if matrix_factorization_choice == '1':
            ###PMF
            plot_embedding2D(u_vec, di_graph=G1, dim_reduc="pca", dataset_name=dataset_name + " PMF (Dense Matrix)")
            plot_embedding2D(u_vec, di_graph=G1, dim_reduc="tsne", dataset_name=dataset_name + " PMF (Dense Matrix)")
        else:
            ###SVD
            plot_embedding2D(u_vec, di_graph=G1, dim_reduc="pca", dataset_name=dataset_name + " SVD (Dense Matrix)")
            plot_embedding2D(u_vec, di_graph=G1, dim_reduc="tsne", dataset_name=dataset_name + " SVD (Dense Matrix)")
    else:
        if matrix_factorization_choice == '1':
            ###PMF
            plot_embedding2D(u_vec, di_graph=G1, dim_reduc="pca", dataset_name=dataset_name + " PMF (Sparse Matrix)")
            plot_embedding2D(u_vec, di_graph=G1, dim_reduc="tsne", dataset_name=dataset_name + " PMF (Sparse Matrix)")
        else:
            ###SVD
            plot_embedding2D(u_vec, di_graph=G1, dim_reduc="pca", dataset_name=dataset_name + " SVD (Sparse Matrix)")
            plot_embedding2D(u_vec, di_graph=G1, dim_reduc="tsne", dataset_name=dataset_name + " SVD (Sparse Matrix)")


dataset_choice, is_show_plot, is_sparse, matrix_factorization_choice, is_classification = user_input()
# application initiator
start_time = time.time()
print_result = []
adj_matrix, dataset_name = data_processing_to_matrix(dataset_choice)
u, v, s, G = matrix_factorization(adj_matrix, is_sparse, matrix_factorization_choice)

if is_classification == 'y':
    matrix_factorization_classification(v, s)
if is_show_plot == 'y':
    show_plot(u, G, dataset_name)
