import matplotlib.pyplot as plt
import networkx as nx
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
import sys
sys.path.insert(0, './')
from gem.utils import plot_util
import datetime


def plot_embedding2D(node_pos, node_colors=None, di_graph=None,dim_reduc="pca", dataset_name=""):
    t = datetime.datetime.now()
    time = t.strftime('%Y%m%d%H%M%S')

    node_num, embedding_dimension = node_pos.shape
    title = ''
    if(embedding_dimension > 2):
        if(dim_reduc == "pca"):
            title = dataset_name+" PCA"
            print("Embedding dimension greater than 2, use PCA to reduce it to 2")
            model = PCA(n_components=2)
            node_pos = model.fit_transform(node_pos)
        else:
            title = dataset_name + " t-SNE"
            print("Embedding dimension greater than 2, use tSNE to reduce it to 2")
            model = TSNE(n_components=2)
            node_pos = model.fit_transform(node_pos)

    if di_graph is None:
        # plot using plt scatter
        plt.scatter(node_pos[:, 0], node_pos[:, 1], c=node_colors)
    else:
        # plot using networkx with edge structure
        pos = {}
        for i in range(node_num):
            pos[i] = node_pos[i, :]
        if node_colors:
            nx.draw_networkx_nodes(di_graph, pos,
                                   node_color=node_colors,
                                   width=0.1, node_size=100,
                                   arrows=False, alpha=0.8,
                                   font_size=5)
        else:
            nx.draw_networkx(di_graph, pos, node_color=node_colors,
                             width=0.1, node_size=300,
                             alpha=0.8, font_size=5)
    plt.title(title)
    #plt.show()
    filename=time+" "+title+".png"
    plt.savefig('images/'+filename,bbox_inches='tight')
    plt.close()

def expVis(X, res_pre, m_summ, node_labels=None, di_graph=None):
    print('\tGraph Visualization:')
    if node_labels:
        node_colors = plot_util.get_node_color(node_labels)
    else:
        node_colors = None
    plot_embedding2D(X, node_colors=node_colors,
                     di_graph=di_graph)
    plt.savefig('%s_%s_vis.pdf' % (res_pre, m_summ), dpi=300,
                format='pdf', bbox_inches='tight')
    plt.figure()