import pandas as pd
import networkx as nx
import csv
import numpy as np
from ProbabilisticMatrixFactorization import PMF
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from PlotGraphVisual import plot_embedding2D
from collections import defaultdict


def import_data_graph(filename, dataset_level="", nodetype=int, weighted=True, delimiter=None):
    if delimiter is not None:
        delimiter = delimiter
        if weighted is True:
            graph = nx.read_weighted_edgelist('../dataset/' + filename,
                                              create_using=nx.DiGraph(),
                                              nodetype=nodetype,
                                              delimiter=delimiter,
                                              encoding='utf-8')
        else:
            graph = nx.read_edgelist('../dataset/' + filename,
                                     create_using=nx.DiGraph(),
                                     delimiter=delimiter,
                                     nodetype=nodetype,
                                     encoding='utf-8')
    else:
        if weighted is True:
            graph = nx.read_weighted_edgelist('../dataset/' + filename,
                                              create_using=nx.DiGraph(),
                                              nodetype=nodetype,
                                              encoding='utf-8')
        else:
            graph = nx.read_edgelist('../dataset/' + filename,
                                     create_using=nx.DiGraph(),
                                     nodetype=nodetype,
                                     encoding='utf-8')

    print(dataset_level + " Graph Node: ", len(graph.nodes()))
    print(dataset_level + " Graph edges: ", len(graph.edges()))
    return graph


def import_labels(filename):
    labels = defaultdict(list)
    with open('../dataset/' + filename, 'r') as f:
        for l in f.readlines():
            node_id, group = _read_label(l)
            labels[node_id].append(group)
    return labels


def import_labels_array(filename):
    reader = csv.reader(open('../dataset/' + filename), delimiter=",")
    x = list(reader)
    result = np.array(x).astype("int32")
    return result


def import_data_adj_matrix(filename, dataset_level="", nodetype=int, weighted=True, delimiter=None):
    graph = import_data_graph(filename, dataset_level=dataset_level, nodetype=nodetype, weighted=weighted,
                              delimiter=delimiter)
    adjacency_matrix = nx.adjacency_matrix(graph)
    return adjacency_matrix, len(graph.nodes()), len(graph.edges())


def import_data_dense_matrix(filename, dataset_level="", nodetype=int, weighted=True, delimiter=None):
    graph = import_data_graph(filename, dataset_level=dataset_level, nodetype=nodetype, weighted=weighted,
                              delimiter=delimiter)
    adjacency_matrix = nx.adjacency_matrix(graph)

    return adjacency_matrix.todense()


def import_data_array(filename, dataset_level="", nodetype=int, weighted=True):
    graph = import_data_graph(filename, dataset_level=dataset_level, nodetype=nodetype, weighted=weighted)
    data_array = nx.to_numpy_recarray(graph)
    return data_array


def _read_label(line_str):
    """Internal function to read a line of group-edges.csv file and
    return two integers for node_id and label_id.
    Arguments:
        line_str: A single line with the format: node_id, group_id
    Returns:
        node_id: An integer indicates the node id
        group_id: An integer indicates the node's group or label
    """
    raw_id, raw_group = line_str.split(',')
    return int(raw_id), int(raw_group)


def tsne_reduction(similarity_matrix):
    one_min_sim = 1 - similarity_matrix
    tsne = TSNE(n_components=2).fit_transform(one_min_sim)
    x_pos, y_pos = tsne[:, 0], tsne[:, 1]
    plt.plot(x_pos, y_pos)
    plt.show()
    return (x_pos, y_pos)

def pad_to_square(a, pad_value=0):
  m = a.reshape((a.shape[0], -1))
  padded = pad_value * np.ones(2 * [max(m.shape)], dtype=m.dtype)
  padded[0:m.shape[0], 0:m.shape[1]] = m
  return padded
