import networkx as nx
import numpy as np
import surprise.prediction_algorithms.matrix_factorization as sp
from pmf_model import PMFM
from ProbabilisticMatrixFactorization import PMF
from pmf import PMFF
from sklearn.model_selection import train_test_split

from scipy.sparse.linalg import svds


def svd_factorization_dense(adj_matrix, full_matrices=True):
    print('SVD training model.......')
    u, s, v = np.linalg.svd(adj_matrix,full_matrices=False)
    print(u.shape)
    print(s.shape)
    print(v.shape)
    print('End.......')
    return u, s, v


def svd_factorization_sparse(adj_matrix, full_matrices=True):
    print('SVD training model.......')
    adj_matrix = adj_matrix.asfptype()
    k= adj_matrix.shape
    u, s, v = svds(adj_matrix,k=int(min(k)/2))
    print(u.shape)
    print(s.shape)
    print(v.shape)
    print('End.......')
    return u, s, v


def pmf_factorization(adj_matrx):
    train, test = train_test_split(adj_matrx, test_size=0.4)
    print('pmf training model.......')
    lambda_alpha = 0.01
    lambda_beta = 0.01
    latent_size = adj_matrx.shape[1]
    lr = 3e-5
    iters = 10
    model = PMFM(R=adj_matrx, lambda_alpha=lambda_alpha, lambda_beta=lambda_beta, latent_size=latent_size, momuntum=0.9,
                 lr=lr,
                 iters=iters, seed=1)
    U, V = model.train(train_data=train, vali_data=test)

    print(U.shape)
    print(V.shape)
    print('End.......')
    return U, V


def pmf_factorization_hailin(adj_matrx):
    pmf = PMF()
    pmf.set_params({"num_feat": 10, "epsilon": 1, "_lambda": 0.1, "momentum": 0.8, "maxepoch": 10, "num_batches": 100,
                    "batch_size": 1000})
    train, test = train_test_split(adj_matrx, test_size=0.2)  # spilt_rating_dat(ratings)
    u, v = pmf.fit(train, test)
    print(u.shape)
    print(v.shape)
    print('End.......')
    return u, v


def pmf_factorization_chy(adj_matrx):
    n_user = adj_matrx.shape[0]
    n_item = adj_matrx.shape[1]
    n_feature = 10
    eval_iters = 20
    print("training PMF model...")
    pmf = PMFF(n_user=n_user, n_item=n_item, n_feature=n_feature,
               epsilon=25., max_rating=5., min_rating=1., seed=0)
    s = pmf.fit(adj_matrx[:n_item], n_iters=eval_iters)
    u = s.user_features_
    v = s.item_features_
    print('End.......')
    return u, v
